package app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MyWindow extends JFrame
{
    private static final int WIDTH = 900;
    private static final int HEIGHT = 650;
    private final JPanel conPane;

    public MyWindow()
    {
        this.setTitle("Moje Okno");
        // definicja zdarzenia zamkniecia okna
        this.addWindowListener	(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                dispose();
                System.exit(0);
            }
        });
        // Rozmieszczenie okna na srodku ekranu
        Dimension frameSize = new Dimension(WIDTH,HEIGHT);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();//pobranie rozdzielczosci pulpitu
        if(frameSize.height > screenSize.height) frameSize.height = screenSize.height;
        if(frameSize.width > screenSize.width) frameSize.width = screenSize.width;
        setSize(frameSize);
        setLocation((screenSize.width-frameSize.width)/2,
                (screenSize.height-frameSize.height)/2);

        // Utworzenie glownego kontekstu (ContentPane)
        conPane = (JPanel) this.getContentPane();
        conPane.setLayout(new BorderLayout());
        CenterPanel centerPanel  = new CenterPanel();
        centerPanel.setVisible(true);
        conPane.add(centerPanel, BorderLayout.CENTER);

    }

    public static void main(String args[])
    {
        System.out.println("Start Aplikacji");
        MyWindow f = new MyWindow();
        f.setVisible(true);
    }
}
