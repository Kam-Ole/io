package app;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.TitledBorder;
/**
 * Program <code>MyWindow</code>
 * Klasa <code>CenterPanel</code> definiujaca centralny panel
 * aplikacji zawierajacy glowna funkcjonalnosc aplikacji
 * @author Kamil Oleksiak
 * @version 1.0
 */
public class CenterPanel extends JPanel implements ActionListener {

    private static final long serialVersionUID = 1L;
    private JPanel southPanel ,centralPanel;
    private JTextArea resultTextArea;
    private JButton wstawButton;


    /**
     * Konstruktor bezparametrowy klasy <CODE>InfoBottomPanel<CODE>
     */
    public CenterPanel() {
        createGUI();
    }
    /**
     * Metoda tworzacaca graficzny interfejs uzyytkownika
     */
    public void createGUI() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // Utworzenie panelu
        centralPanel = createCentralPanel();
        southPanel = createSouthPanel();

        // Utworzenie obiektow
        this.add(centralPanel);
        this.add(southPanel);
        centralPanel.setVisible(true);
        southPanel.setVisible(true);
    }
    /**
     * Metoda tworzaca panel z parametrami
     */

    public JPanel createCentralPanel(){

        JPanel jp = new JPanel();

        wstawButton = new JButton("Wstaw");
        wstawButton.addActionListener(this);
        jp.add(wstawButton);

        return jp;
    }
    /**
     * Metoda tworzaca panel z wynikami
     */
    public JPanel createSouthPanel() {
        JPanel jp = new JPanel();
        jp.setLayout(new BorderLayout());

        resultTextArea = new JTextArea();
        resultTextArea.append("Start aplikacji\n");
        jp.add(resultTextArea,BorderLayout.CENTER);
        return jp;
    }
    /**
     * Metoda obsługujaca zdarzenie akcji
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == wstawButton) {

                resultTextArea.append("Button used");//wypisanie danych w polu textowym
        }

    }

    /**
     * Metoda okreslajaca wartosci odstepow od krawedzi panelu
     * (top,left,bottom,right)
     */
    public Insets getInsets() {
        return new Insets(5,10,10,10);
    }
}
